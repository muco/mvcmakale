﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilimselYayin.Models
{
    public class HomeModel
    {
        public List<MakaleTuru> MakaleTur { get; set; }
        public List<Makale> Makale { get; set; }
        public List <Yazar> Yazar { get; set; }
        public  List <Slider> _Slider { get; set; }
    }
}