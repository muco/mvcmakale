﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BilimselYayin.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BilimselYayin.Areas.Admin.Controllers
{
    public class YonetimController : Controller
    {
        // GET: Admin/Yonetim
        ApplicationDbContext context = new ApplicationDbContext();
        public ActionResult Index()
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var model = roleManager.Roles.ToList();
            return View(model);
        }
    }
}