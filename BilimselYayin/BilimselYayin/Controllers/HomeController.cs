﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BilimselYayin.Models;

namespace BilimselYayin.Controllers
{

    public class HomeController : Controller
    {

        BmakaleEntities db = new BmakaleEntities();
        HomeModel HomeModel = new HomeModel();

        public ActionResult Index()
        {

            HomeModel.MakaleTur = db.MakaleTuru.ToList();
            return View(HomeModel);


        }

        [ChildActionOnly]
        public ActionResult _Slider()
        {
            HomeModel._Slider = db.Slider.ToList();
            return View(HomeModel);
        }

        public ActionResult Yazar()
        {
            HomeModel.Yazar = db.Yazar.ToList();
            return View(HomeModel);
        }

        public ActionResult Makale()
        {
            HomeModel.Makale = db.Makale.ToList();
            HomeModel.MakaleTur = db.MakaleTuru.ToList();
            return View(HomeModel);
        }


        [HttpPost]  
        public ActionResult Makale(string arama)
        {
            HomeModel.Makale = db.Makale.Where(x => x.MIcerik.Contains(arama)).ToList();
            return View(HomeModel);
        }
        public ActionResult Detay(int id)
        {
            var model = db.Makale.Find(id);
            return View(model);
        }


        public ActionResult iletisim()
        {
            return View();
        }


        public ActionResult Olustur()
        {
            ViewBag.YazarID = new SelectList(db.Yazar.ToList(), "YazarID", "YAdi");
            ViewBag.TurID = new SelectList(db.MakaleTuru.ToList(),"TurID","TurAdi");
            return View();
        }

        [HttpPost]
        public ActionResult Olustur(Makale model)
        {
            model.YayimTarihi = DateTime.Now;
            if (ModelState.IsValid)
            {
            db.Makale.Add(model);
            db.SaveChanges();
            }


            return RedirectToAction("Index");
        }

        public ActionResult Duzenle(int id)
        {
            ViewBag.YazarID = new SelectList(db.Yazar.ToList(), "YazarID", "YAdi");
            ViewBag.TurID = new SelectList(db.MakaleTuru.ToList(), "TurID", "TurAdi");
            var model=db.Makale.Find(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Duzenle(Makale model)
        {
            if (ModelState.IsValid)
            {
                var kayit = db.Makale.Find(model.MakaleID);
                TryUpdateModel(kayit);
            db.SaveChanges();
                ViewBag.KayitBasarili = true;
            }
            else
            {
                ViewBag.KayitBasarili = false;
            }
            ViewBag.YazarID = new SelectList(db.Yazar.ToList(), "YazarID", "YAdi");
            ViewBag.TurID = new SelectList(db.MakaleTuru.ToList(), "TurID", "TurAdi");

            return View();
        }

        public ActionResult Sil(int id)
        {
            var model=db.Makale.Find(id);
            return View(model);
        }



        [HttpPost]
        public ActionResult SilOnay(int id)
        {
            if (ModelState.IsValid)
            {
            var model = db.Makale.Find(id);
            db.Makale.Remove(model);
            db.SaveChanges();
            }


            return RedirectToAction("Index");
        }
    }
}